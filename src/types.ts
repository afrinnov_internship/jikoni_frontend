export type ResponseType<T> = {
    data: T
}

export type RecipeType = {
    recipeCode: string,
    recipeLabel: string,
    recipeDescription: string,
    recipeImageUrl: string
}

export type RecipeDetailsType = {
    recipeCode: string,
    recipeLabel: string,
    recipeDescription: string,
    recipeImageUrl: string,
    recipeAuthor: string,
    recipeModificationDate: Date,
    recipeIngredients : IngredientType[]
}

export type RecipeDto = {
    recipeCode: string,
    recipeLabel: string,
    recipeDescription: string,
    recipeImageUrl: string,
    recipeAuthor: string,
    recipeIngredients: IngredientType[],
    recipeModificationDate: Date
}

export type RecipeFormType = {
    recipeLabel: string,
    recipeDescription: string,
    recipeImageUrl: string,
    recipeIngredients : IngredientType[]
}

export type DataList = {
    data: {
        content: RecipeType[],
        number: number,
        size: number,
        totalPages: number
    },
    errorCode: number,
    message: string,
    lang: string    
}

export type DataDetail = {
    data: RecipeDetailsType,
    errorCode: number,
    message: string,
    lang: string    
}

/* export type IngredientFormType = {
    ingredientName: string,
    ingredientQuantity: string,
    ingredientUnit: string
} */

export type IngredientType = {
    ingredientCode: string,
    ingredientName: string,
    ingredientQuantity: number,
    ingredientUnit: string
}

export type CommentFormType = {
    commentDateCreated: Date,
    commentDescription: string
}