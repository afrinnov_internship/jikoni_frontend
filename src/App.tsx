import { QueryClient, QueryClientProvider } from "react-query";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Container from "./components/Container/Container";
import { GlobalStateProvider } from "./context/GlobalState";

import Home from "./pages/Home";
import RecipeAdd from "./pages/RecipeAdd";
import RecipeAddTest from "./pages/RecipeAddTest";
import RecipeDetails from "./pages/RecipeDetails";
import RecipesList from "./pages/RecipesList";

const queryClient = new QueryClient();

export default function App() {  
    return (
        <QueryClientProvider client={queryClient}>
            <GlobalStateProvider>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<Container />}>
                            <Route path="" element={<Home />} />
                            <Route path="list-recipes/:page" element={<RecipesList />} />
                            <Route path="recipes/:code" element={<RecipeDetails />} />
                            <Route path="add-recipe" element={<RecipeAdd />} />
                        </Route>
                    </Routes>
                </BrowserRouter>
            </GlobalStateProvider>
        </QueryClientProvider>
    )
}