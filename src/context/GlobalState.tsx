import { createContext, PropsWithChildren, useState } from "react";

type GlobalStateType = {
    pageSize: number;
    search: string;
}

type GlobalStateDispatchType = {
    pageSize?: number;
    search?: string;
}

type GlobalStateContextType = {
    value: GlobalStateType;
    dispatch: (oldState: GlobalStateDispatchType) => void;
}

const initState: GlobalStateType = {
    pageSize: 12,
    search: ""
}

export const GlobalStateContext = createContext<GlobalStateContextType>({
    value: initState,
    dispatch() {
        
    },
})

export const GlobalStateProvider = ({children}: PropsWithChildren) => {
    const [value, setState] = useState(initState);

    const handleChange = (value: GlobalStateDispatchType) => setState(old => ({...old, ...value}))

    return (
        <GlobalStateContext.Provider value={{value, dispatch: handleChange}}>
            {children}
        </GlobalStateContext.Provider>
    )
}