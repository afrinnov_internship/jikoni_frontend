import { DataDetail, DataList, RecipeDetailsType, RecipeFormType, RecipeType, ResponseType } from "../types";
import apiAxiosInstance from "./config";

export const recipesList = async (pageNumber: number, pageSize: number): Promise<RecipeType[]> => {
    const response = await apiAxiosInstance.get<DataList>(`/${pageNumber}/${pageSize}`);
    return response.data.data.content
}

export const recipeDetails = async (code: string): Promise<RecipeDetailsType> => {
    const response = await apiAxiosInstance.get<DataDetail>(`/detail/${code}`);
    console.log(response.data.data)
    return response.data.data
}

export const recipeAdd = async (form: RecipeFormType): Promise<RecipeDetailsType> => {
    const response = await apiAxiosInstance.post<DataDetail>("/create", form);
    return response.data.data
}