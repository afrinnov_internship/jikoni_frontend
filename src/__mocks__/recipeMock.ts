import { mock } from "../api/config";
import { RecipeDetailsType, RecipeFormType } from "../types";

const fakeData = [
    {
        recipeCode: "12",
        recipeLabel: "Tarte Fraise",
        recipeDescription: "Faire la pâte sablée en mélangeant les ingrédients. L'étaler dans un moule. Piquer avec une fourchette. La faire cuire pendant...",
        recipeImageUrl: "https://liliebakery.fr/wp-content/uploads/2021/04/Tarte-aux-fraises-cyril-lignac-Lilie-Bakery-500x500.jpg"
    },
    {
        recipeCode: "13",
        recipeLabel: "Tarte à l'orange",
        recipeDescription: "Etaler ou dérouler la pâte sablée ou brisée et la disposer au fond du moule. Couper les oranges sauf une en très fines rondelles. Préchauffer le four à 170 °C...",
        recipeImageUrl: "https://img.cuisineaz.com/610x610/2014/03/18/i93668-tarte-a-l-orange-sucre.jpg"
    },
    {
        recipeCode: "14",
        recipeLabel: "Gâteau au yaourt",
        recipeDescription: "Préchauffer le four à 180°C (thermostat 6). Mélanger tout simplement les ingrédients un à un en suivant l'ordre suivant : levure, yaourt, huile, sucre, farine, oeufs et zeste de citron. Beurrer ou huilez un moule à manqué à l'aide d'une feuille...",
        recipeImageUrl: "https://assets.afcdn.com/recipe/20200828/113345_w1024h768c1cx2880cy1920.jpg"
    },
    {
        recipeCode: "15",
        recipeLabel: "Gâteau au yaourt chocolaté",
        recipeDescription: "Dans un saladier, fouettez au batteur électrique les œufs et le sucre pendant 3 minutes jusqu’à ce que la préparation blanchisse et devienne mousseuse. Cassez le chocolat en morceaux et faites-le fondre au bain-marie ou au micro-ondes...",
        recipeImageUrl: "https://i.pinimg.com/736x/c1/df/24/c1df24a8bf21fdff9657bea10943fdb6.jpg"
    },
    {
        recipeCode: "16",
        recipeLabel: "Recette de Fondant au chocolat, coeur de caramel au beurre demi-sel, coulis de framboises",
        recipeDescription: "1. POUR LA FARCE \n" +
        "Placer le sucre dans une poêle ou une casserole large. Faire chauffer à feu vif et ne commencer à mélanger que lorsqu'on obtient un début de couleur caramel. Remuer alors vivement avec une spatule jusqu'à ce que le caramel soit bien homogène...",
        recipeImageUrl: "https://www.atelierdeschefs.com/media/recette-e22147-fondant-au-chocolat-coeur-de-caramel-au-beurre-demi-sel-coulis-de-framboises.jpg"
    },
    {
        recipeCode: "17",
        recipeLabel: "ERU",
        recipeDescription: "Lavez et découpez tous vos légumes (Eru). Après avoir découpé et lavé la peau de bœuf, faites la bouillir pendant 30 minutes ensuite ajouter la viande de bœuf préalablement nettoyée et découpée. N’oubliez pas d’y ajouter du sel...",
        recipeImageUrl: "https://cuisinedumboa.com/wp-content/uploads/2019/03/FB_IMG_15307324283946846-1.jpg"
    },
    {
        recipeCode: "18",
        recipeLabel: "Taro avec sa sauce jaune",
        recipeDescription: "Fais cuire le taro en robe des champs dans une casserole pendant environ 2h. Épluche les taros et pile-les pendant qu'ils sont encore chauds. Assure-toi qu'il n'y a plus de grumeaux en les écrasant, la pâte doit être lisse. Ajoute un peu d’eau à...",
        recipeImageUrl: "https://reglo.org/admin/posts_images/5901_6_2017-03-14consommons-local-tous-dans-la-sauce-jaune.jpg"
    },
    {
        recipeCode: "19",
        recipeLabel: "Okok sucré",
        recipeDescription: "Préparez vos feuilles d’okok et réservez. Versez le suc de noix de palme, l’eau et le sel. Laissez cuire pendant 5 à 10 minutes. Ajoutez la pâte d’arachide...",
        recipeImageUrl: "http://cdn.shopify.com/s/files/1/0046/8687/2649/articles/20180428-224841_grande_da48bffe-31ad-4658-b9af-383f745ea8b3.jpg?v=1550656688"
    },
    {
        recipeCode: "20",
        recipeLabel: "Ndolè",
        recipeDescription: "Mettre l’eau salée à bouillir, découper et laver les feuilles de ndole. Les mettre dans de l’eau bouillante et les cuire rapidement à découvert pendant 45mn environ avec un morceau de sel gemme. Rincer et égoutter dans une passoire. Presser les feuilles pour enlever de l’eau, les mettre...",
        recipeImageUrl: "https://s3-eu-west-1.amazonaws.com/images-ca-1-0-1-eu/recipe_photos/original/85533/_ndolee.png"
    },
    {
        recipeCode: "21",
        recipeLabel: "Poulet basquaise",
        recipeDescription: "Épluchez les oignons et les gousses d'ail. Lavez soigneusement les cuisses de poulet ainsi que les poivrons et les tomates. Dans une marmite, rissolez les morceaux de...",
        recipeImageUrl: "https://img.cuisineaz.com/660x660/2015/07/31/i95030-poulet-basquaise.jpg"
    },
    {
        recipeCode: "22",
        recipeLabel: "Poisson braisé",
        recipeDescription: "Nettoyer et laver minutieusement les poissons. Hacher un oignon, le mélanger avec une gousse d’ail écrasée et le gingembre. Incorporer le piment, un cube d’assaisonnement et mélanger avec 3 c.à.s d’huile. Enduire ce mélange à l’intérieur...",
        recipeImageUrl: "https://www.recetteaz.net/wp-content/uploads/2020/01/Recette-Poisson-brais%C3%A9-1024x768.jpg"
    },
    {
        recipeCode: "23",
        recipeLabel: "Couscous algérien",
        recipeDescription: "Faites revenir les morceaux d’agneau avec un filet d’huile d’olive dans la marmite du couscoussier. Préchauffez le four à 180°C. Ajoutez les oignons mixés et laissez dorer pendant 10 minutes en retournant les morceaux de viande d'agneau...",
        recipeImageUrl: "https://fac.img.pmdstatic.net/fit/https.3A.2F.2Fi.2Epmdstatic.2Enet.2Ffac.2F2022.2F01.2F27.2Fc61aaf77-2e2a-4970-80db-0669a2429a6b.2Ejpeg/750x562/quality/80/crop-from/center/cr/wqkgSVNUT0NLL0dFVFRZIElNQUdFUyAvIEZlbW1lIEFjdHVlbGxl/couscous-algerien.jpeg"
    }
]
const fakeRecipe = {
    recipeCode: "12",
    recipeLabel: "Tarte Fraise",
    recipeDescription: "Faire la pâte sablée en mélangeant les ingrédients. " +
                        "L'étaler dans un moule. Piquer avec une fourchette. " +
                        "La faire cuire pendant environ 20 minutes à 180 °C. " +
                        
                        "Faire chauffer le lait. Battre les jaunes d'oeuf et " +
                        "le sucre, et ajouter la farine. Verser le lait bouillant " +
                        "tout en remuant. Une fois que la crème a épaissie, retirer " +
                        "du feu, puis mettre le beurre en fouettant vivement. Laisser " +
                        "refroidir." +

                        "Laver les fraises et les couper dans le sens de la longueur. " +
                        "Sur le fond de pâte (refroidie), déposer la crème pâtissière, " +
                        "puis les fraises. A déguster frais. Bon appétit ! ",
    recipeImageUrl: "https://liliebakery.fr/wp-content/uploads/2021/04/Tarte-aux-fraises-cyril-lignac-Lilie-Bakery-500x500.jpg",
    recipeAuthor: "Shany Marion",
    recipeIngredients: [
        {
            ingredientCode: "1",
            ingredientName: "Farine",
            ingredientQuantity: "250",
            ingredientUnit: "g"
        },
        {
            ingredientCode: "2",
            ingredientName: "Sucre",
            ingredientQuantity: "180",
            ingredientUnit: "g"
        },
        {
            ingredientCode: "3",
            ingredientName: "Beurre",
            ingredientQuantity: "150",
            ingredientUnit: "g"
        },
        {
            ingredientCode: "4",
            ingredientName: "Sel",
            ingredientQuantity: "1",
            ingredientUnit: "pincée"
        },
        {
            ingredientCode: "5",
            ingredientName: "Oeufs",
            ingredientQuantity: "4",
            ingredientUnit: ""
        },
        {
            ingredientCode: "6",
            ingredientName: "Lait",
            ingredientQuantity: "50",
            ingredientUnit: "cl"
        },
        {
            ingredientCode: "7",
            ingredientName: "Fraise",
            ingredientQuantity: "250",
            ingredientUnit: "g"
        }
    ] 
}
let recipesFakeData: RecipeDetailsType[] = [
    { 
        recipeCode: "",
        recipeLabel: "",
        recipeDescription: "",
        recipeImageUrl: "",
        recipeAuthor: "",
        recipeModificationDate: new Date(),
        recipeIngredients: []
    }
]
mock.onGet("/recipes").reply(200, {
    data: fakeData,
});

mock.onGet("/detail/:code").reply(200, {
    data: fakeRecipe,
});

mock.onPost("/create").reply<RecipeFormType>((config) => {
    const recipeForm: RecipeFormType = config.data;
    const recipe: RecipeDetailsType = {
        recipeCode: new Date().getTime().toString(),
        recipeLabel: recipeForm.recipeLabel,
        recipeDescription: recipeForm.recipeDescription,
        recipeImageUrl: recipeForm.recipeImageUrl,
        recipeAuthor: "",
        recipeIngredients: recipeForm.recipeIngredients,
        recipeModificationDate: new Date()
    };
    
    recipesFakeData = [...recipesFakeData, recipe]
 
    return [200, {data: recipe}]
})
  