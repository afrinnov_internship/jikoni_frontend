import { Menu } from "antd";
import { Header } from "antd/lib/layout/layout";
import { useEffect } from "react";
import { useContext } from "react";
import { Link, useLocation, useParams } from "react-router-dom";
import { GlobalStateContext } from "../../context/GlobalState";

export default function NavBar() {
    const param = useParams();
    const location = useLocation();
    const {dispatch} = useContext(GlobalStateContext);
    const routes = [
        {path: "/", name: "Accueil"},
        {path: "/add-recipe", name: "Ajouter une recette"},
        {path: "/list-recipes/1", name: "Lister des recettes"},
        {path: "/log-out", name: "Déconnexion"},
    ]

    useEffect(() => {
        dispatch({pageSize: 20})
    }, [])
    return (
        <Header>
            <img src="logo.jpg" style={{float: "left", width: "90px", height: "40px", margin: "16px 24px 16px 0"}}></img>
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={["/"]}
                selectedKeys={[location.pathname]}
                multiple={false}
                items={routes.map((route) => ({
                    label: (
                        <Link to={route.path} key={route.path}>                        
                            {route.name}
                        </Link>
                    ),
                    key: route.path
                }))}
            />
        </Header>
    )
}
