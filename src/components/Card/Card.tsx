import { Button, Card as CardAntd } from "antd"
import Meta from 'antd/lib/card/Meta';
import _ from "lodash";
import { useNavigate } from "react-router-dom";
import { RecipeType } from "../../types";

interface Props {
    item: RecipeType
}

export default function Card({item}: Props) {
    let navigate = useNavigate();
    return (
        <CardAntd
            cover={
                <img
                    alt="example"
                    src={item.recipeImageUrl}
                    style={{
                        height: "250px",
                        objectFit: "cover",
                    }}
                />
            }
            actions={[
                <Button  type="primary" onClick={() => navigate(`/recipes/${item.recipeCode}`)} style={{width: "calc(100% - 20px)"}}>Détail</Button>,
                <Button type="primary" style={{width: "calc(100% - 20px)"}}>Supprimer</Button>
            ]}
            style={{
                height: "100%"
            }}
        >
            <Meta
                title={item.recipeLabel}
                description={_.truncate(item.recipeDescription, {length: 140, separator: ' '})}
                style={{
                    minHeight: "165px"
                }}
            />
        </CardAntd>

    )
}