import { Footer } from "antd/lib/layout/layout";

export default function FooterBar() {
    return (
        <Footer style={{ textAlign: 'center' }}>Jikoni Design ©2022 Created by Shany Marion</Footer>
    )
}