import { Col, Row, Skeleton } from "antd";
import _ from "lodash";

export const SkeletonHomeRecipesList = () => (
    <Row gutter={[25,40]}>{
        _.range(0,8).map((index: number) => (
            <Col key={index} span={6}>
                <Skeleton.Image />
            </Col>
        ))
    }
    </Row>
)