import { Input } from "antd";

export default function SearchBar() {
    const { Search } = Input;
    const onSearch = (value: string) => console.log(value);

    return (
        <Search 
        placeholder="input search text" 
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch} 
        style={{ width: 400, marginBottom: "20px", position: "relative", left: "400px" }} 
        />
    )
}