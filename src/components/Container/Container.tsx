import { Layout } from "antd";
import { Outlet } from "react-router";
import FooterBar from "../Footer/FooterBar";
import NavBar from "../Nav/NavBar";

export default function Container() {
    return (
        <Layout style={{minHeight: "100vh"}}>
            <NavBar />
            <Outlet />
            <FooterBar />
        </Layout>
    )
}