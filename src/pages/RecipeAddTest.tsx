import { SmileOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Button, Form, Input, InputNumber, Modal, Typography } from 'antd';
import type { FormInstance } from 'antd/es/form';
import React, { useEffect, useRef, useState } from 'react';
import { IngredientType } from '../types';

const layout = {
labelCol: { span: 8 },
wrapperCol: { span: 16 },
};
const tailLayout = {
wrapperCol: { offset: 8, span: 16 },
};

interface UserType {
name: string;
age: string;
}

interface ModalFormProps {
visible: boolean;
onCancel: () => void;
}
  
// reset form fields when modal is form, closed
const useResetFormOnCloseModal = ({ form, visible }: { form: FormInstance; visible: boolean }) => {
    const prevVisibleRef = useRef<boolean>();
    useEffect(() => {
        prevVisibleRef.current = visible;
    }, [visible]);
    const prevVisible = prevVisibleRef.current;

    useEffect(() => {
        if (!visible && prevVisible) {
        form.resetFields();
        }
    }, [form, prevVisible, visible]);
};

const ModalForm: React.FC<ModalFormProps> = ({ visible, onCancel }) => {
    const [form] = Form.useForm();

    useResetFormOnCloseModal({
        form,
        visible,
    });

    const onOk = () => {
        console.log(form)
        form.submit();
    };

    return (
        <Modal title="Ajouter un ingrédient" visible={visible} onOk={onOk} onCancel={onCancel}>
        <Form form={form} layout="vertical" name="userForm">
          <Form.Item name="ingredientName" label="Nom" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="ingredientQuantity" label="Quantité" rules={[{ required: true }]}>
            <InputNumber />
          </Form.Item>
          <Form.Item name="ingredientUnit" label="Unité" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    );
};

export default function RecipeAddTest() {
    const [visible, setVisible] = useState(false);

    const showUserModal = () => {
        setVisible(true);
    };

    const hideUserModal = () => {
        setVisible(false);
    };

    const onFinish = (values: any) => {
        console.log('Finish:', values);
    };

    return (
        <Form.Provider
        onFormFinish={(name, { values, forms }) => {
            console.log(forms)
            if (name === 'userForm') {
            const { basicForm } = forms;
            const users = basicForm.getFieldValue('users') || [];
            basicForm.setFieldsValue({ users: [...users, values] });
            setVisible(false);
            }
        }}
        >
        <Form {...layout} name="basicForm" onFinish={onFinish}>
            <Form.Item name="group" label="Group Name" rules={[{ required: true }]}>
            <Input />
            </Form.Item>
            <Form.Item
            label="User List"
            shouldUpdate={(prevValues, curValues) => prevValues.users !== curValues.users}
            >
            {({ getFieldValue }) => {
                const users: IngredientType[] = getFieldValue('users') || [];
                console.log(users)
                return users.length ? (
                <ul>
                    {users.map((user, index) => (
                    <li key={index} className="user">
                        <Avatar icon={<UserOutlined />} />
                        {user.ingredientName} - {user.ingredientQuantity}
                    </li>
                    ))}
                </ul>
                ) : (
                <Typography.Text className="ant-form-text" type="secondary">
                    ( <SmileOutlined /> No user yet. )
                </Typography.Text>
                );
            }}
            </Form.Item>
            <Form.Item {...tailLayout}>
            <Button htmlType="submit" type="primary">
                Submit
            </Button>
            <Button htmlType="button" style={{ margin: '0 8px' }} onClick={showUserModal}>
                Add User
            </Button>
            </Form.Item>
        </Form>

        <ModalForm visible={visible} onCancel={hideUserModal} />
        </Form.Provider>
    );
}