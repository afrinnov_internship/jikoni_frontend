import { Layout, Space, Typography, Image, Descriptions, Tooltip, List, Comment, Avatar, Button, Form, Input, Divider } from "antd";
import { Content } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import Title from "antd/lib/typography/Title";
import { EditOutlined } from '@ant-design/icons';
import { useQuery } from "react-query";
import moment from 'moment';
import { recipeDetails } from "../api/recipeApi";
import { useState } from "react";
import { useParams } from "react-router";

const { Text } = Typography;
const { TextArea } = Input;

interface CommentItem {
  author: string;
  avatar: string;
  content: React.ReactNode;
  datetime: string;
}

interface EditorProps {
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
  onSubmit: () => void;
  submitting: boolean;
  value: string;
}

const CommentList = ({ comments }: { comments: CommentItem[] }) => (
  <List
    dataSource={comments}
    header={`${comments.length} ${comments.length > 1 ? 'commentaires' : 'commentaire'}`}
    itemLayout="horizontal"
    renderItem={props => <Comment {...props} />}
  />
);

const Editor = ({ onChange, onSubmit, submitting, value }: EditorProps) => (
  <>
    <Form.Item>
      <TextArea rows={4} onChange={onChange} value={value} />
    </Form.Item>
    <Form.Item>
      <Button htmlType="submit" loading={submitting} onClick={onSubmit} type="primary">
        Commenter
      </Button>
    </Form.Item>
  </>
);

export default function RecipeDetails() {
  const param = useParams();
    const {data} = useQuery(["/detail/", param.code], () => recipeDetails(param.code ?? ""));
    const [comments, setComments] = useState<CommentItem[]>([]);
    const [submitting, setSubmitting] = useState(false);
    const [value, setValue] = useState('');

    const handleSubmit = () => {
        if (!value) return;

        setSubmitting(true);

        setTimeout(() => {
        setSubmitting(false);
        setValue('');
        setComments([
            ...comments,
            {
            author: 'Shany Marion',
            avatar: 'https://joeschmoe.io/api/v1/random',
            content: <p>{value}</p>,
            datetime: moment().fromNow(),
            },
        ]);
        }, 1000);
    };

    const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setValue(e.target.value);
    };

    return (
        <Layout style={{display: "flex", flexDirection: "row", paddingTop: '5px'}}>
            <Sider style={{paddingTop: '5px', paddingLeft: '5px'}}>  
                <Space direction="vertical">
                    <Title level={4} type="warning"  underline>Ingrédients</Title>{
                        data?.recipeIngredients.map((ingredient) => (
                            <Text style={{color: "white"}}>{ingredient.ingredientName + " : " + ingredient.ingredientQuantity + " " + ingredient.ingredientUnit}</Text>
                        ))
                    }
                </Space>
            </Sider>
            <Content style={{display: "flex", flexDirection: "column"}}>                  
                <Space direction="horizontal">
                    <Image
                        style={{paddingLeft: "10px", paddingRight: "10px"}}
                        width={200}
                        src={data?.recipeImageUrl}
                    />
                    <Title level={2}>{data?.recipeLabel.toUpperCase()}</Title>
                    <Button>
                      <EditOutlined />
                    </Button>
                </Space> 
                <Title style={{padding: "10px"}} level={4}>{"Réalisée par : " + data?.recipeAuthor}</Title>
                <Descriptions style={{padding: "10px"}}>
                    <Descriptions.Item>
                        {data?.recipeDescription}
                    </Descriptions.Item>
                </Descriptions>
                <Divider />
                <Layout style={{margin: "10px"}}>
                    {comments.length > 0 && <CommentList comments={comments} />}
                    <Comment
                        avatar={<Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo" />}
                        content={
                        <Editor
                            onChange={handleChange}
                            onSubmit={handleSubmit}
                            submitting={submitting}
                            value={value}
                        />
                        }
                    />
                </Layout>
            </Content>
            
        </Layout>
    )
}