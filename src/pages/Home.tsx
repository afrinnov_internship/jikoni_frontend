import { Carousel, Image } from "antd";
import { Content } from "antd/lib/layout/layout";
import Item from "antd/lib/list/Item";
import Paragraph from "antd/lib/typography/Paragraph";
import Title from "antd/lib/typography/Title";
import Typography from "antd/lib/typography/Typography";

const contentStyle: React.CSSProperties = {
    height: '160px',
    //color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    //background: '#364d79',
};
  
export default function Home() {
    return (
        <Content style={{ padding: '50px' }}>
            <Title level={2} style={{textAlign: "center", paddingTop: '10px'}}>SITE DE RECETTES</Title>
            <Carousel autoplay>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://liliebakery.fr/wp-content/uploads/2021/04/Tarte-aux-fraises-cyril-lignac-Lilie-Bakery-500x500.jpg"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://img.cuisineaz.com/610x610/2014/03/18/i93668-tarte-a-l-orange-sucre.jpg"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://assets.afcdn.com/recipe/20200828/113345_w1024h768c1cx2880cy1920.jpg"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://www.atelierdeschefs.com/media/recette-e22147-fondant-au-chocolat-coeur-de-caramel-au-beurre-demi-sel-coulis-de-framboises.jpg"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://cuisinedumboa.com/wp-content/uploads/2019/03/FB_IMG_15307324283946846-1.jpg"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://reglo.org/admin/posts_images/5901_6_2017-03-14consommons-local-tous-dans-la-sauce-jaune.jpg"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="http://cdn.shopify.com/s/files/1/0046/8687/2649/articles/20180428-224841_grande_da48bffe-31ad-4658-b9af-383f745ea8b3.jpg?v=1550656688"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://s3-eu-west-1.amazonaws.com/images-ca-1-0-1-eu/recipe_photos/original/85533/_ndolee.png"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://img.cuisineaz.com/660x660/2015/07/31/i95030-poulet-basquaise.jpg"
                        />
                    </h3>
                </Item>
                <Item>
                    <h3 style={contentStyle}>
                        <Image
                            width={200}
                            src="https://www.recetteaz.net/wp-content/uploads/2020/01/Recette-Poisson-brais%C3%A9-1024x768.jpg"
                        />
                    </h3>
                </Item>
            </Carousel>

            <Typography>
                <Paragraph>
                    <blockquote>
                        <Title level={4}>Notre mission</Title>
                    </blockquote>
                    Passionez de cuisine, trouvez des recettes pour satisfaire vos proches
                </Paragraph>
            </Typography>
        </Content>
    )
}