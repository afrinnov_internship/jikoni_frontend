import { Form, Input, Button, FormInstance, InputNumber, Modal, Typography, Select, Upload, message } from "antd";
import { Content } from "antd/lib/layout/layout";
import { useRef, useEffect, useState, useCallback, FormEvent, ChangeEvent, SelectHTMLAttributes } from "react";
import { CloseOutlined, UploadOutlined  } from '@ant-design/icons';
import Title from "antd/lib/typography/Title";

import { IngredientType, RecipeFormType } from "../types";
import { recipeAdd } from "../api/recipeApi";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

interface ModalFormProps {
  visible: boolean;
  onCancel: () => void;
}

const useResetFormOnCloseModal = ({ form, visible }: { form: FormInstance; visible: boolean }) => {
  const prevVisibleRef = useRef<boolean>();
  useEffect(() => {
      prevVisibleRef.current = visible;
  }, [visible]);
  const prevVisible = prevVisibleRef.current;

  useEffect(() => {
      if (!visible && prevVisible) {
      form.resetFields();
      }
  }, [form, prevVisible, visible]);
};
  
const ModalForm: React.FC<ModalFormProps> = ({ visible, onCancel }) => {
  const [form] = Form.useForm();

  useResetFormOnCloseModal({
    form,
    visible,
  });

  const onOk = () => {
    //console.log(form)
    form.submit();
  };

  return (
    <Modal title="Ajouter un ingrédient" visible={visible} onOk={onOk} onCancel={onCancel}>
      <Form form={form} layout="vertical" name="ingredientForm">
        <Form.Item name="ingredientName" label="Nom" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="ingredientQuantity" label="Quantité" rules={[{ required: true }]}>
          <InputNumber />
        </Form.Item>
        <Form.Item name="ingredientUnit" label="Unité" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};
const normFile = (e: any) => {
  //console.log('Upload event:', e);
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};

export default function RecipeAdd() {
    //const {data, isLoading} = useQuery("/recipes", () => recipesList());
  const [loading, setLoading] = useState(false);
  const [form, setForm] = useState<RecipeFormType>({
    recipeLabel: "",
    recipeDescription: "",
    recipeImageUrl: "",
    recipeIngredients: []
  });

  const handleFormSubmit = useCallback(async (event: FormEvent) => {
    try {
      event.preventDefault();
      setLoading(true);
      //console.log(form)
      const response = await recipeAdd(form);
      //console.log(response);
      console.log(response.recipeCode);
      if(response.recipeCode){
        message.info(" succesfully Created");
      }
    } catch (error) {
      message.error("recipe was not created check your form")
    } finally{
      setLoading(false)
    }
  }, [form])
  
  const handleFormChange = useCallback((event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement >) => {
    setForm((oldForm) => ({...oldForm, [event.target.name]: event.target.value}))
  }, []);

  const [visible, setVisible] = useState(false);

  const showIngredientModal = () => {
      setVisible(true);
  };

  const hideIngredientModal = () => {
      setVisible(false);
  };

  const onFinish = (values: any) => {
    //console.log('Finish:', values);
    form.recipeImageUrl = values["recipeImageUrl"]["0"]["name"]
    //form.recipeImageUrl = values
  };

  return (
    <Content style={{ paddingRight: '30px' }}>
      <Title level={3} style={{textAlign: "center", paddingTop: '10px'}} underline>Formulaire d'ajout d'une recette</Title>
      <Form.Provider
        onFormFinish={(name, { values, forms }) => {
          //console.log(values)
          if (name === 'ingredientForm') {
            const { recipeForm } = forms;
            const recipes = recipeForm.getFieldValue('recipes') || [];
            recipeForm.setFieldsValue({ recipes: [...recipes, values] });
            setVisible(false);
          }
        }}
      >
        <Form {...layout} name="recipeForm" onFinish={onFinish} style={{paddingTop: '30px'}} onSubmitCapture={handleFormSubmit}>
          <Form.Item
          name="recipeImageUrl"
          label="Image"
          valuePropName="fileList"
          getValueFromEvent={normFile}
          rules={[{ required: true }]}
          >
            <Upload listType="picture">
              <Button icon={<UploadOutlined />}>Sélectionnez une image</Button>
            </Upload>
          </Form.Item>

          <Form.Item label="Libellé" rules={[{ required: true }]}>
            <Input name="recipeLabel" value={form.recipeLabel} onChange={handleFormChange} disabled={loading} />
          </Form.Item>

          <Form.Item
          //name={"recipeIngredients"}
          rules={[{ required: true }]}
          label="Liste des ingrédients"
          shouldUpdate={(prevValues, curValues) => prevValues.ingredients !== curValues.ingredients}
          >
            {({ getFieldValue }) => {
              const ingredients: IngredientType[] = getFieldValue('recipes') || [];
              //console.log(ingredients)
              form.recipeIngredients = ingredients;
              return ingredients.length ? (
                <ul>
                  {ingredients.map((ingredient, index) => (
                    <li key={index} className="ingredient">
                      {ingredient.ingredientName} : {ingredient.ingredientQuantity + ' ' + ingredient.ingredientUnit}
                    </li>
                  ))}
                </ul>
              ) : (
                <Typography.Text className="ant-form-text" type="secondary">
                  ( <CloseOutlined /> Pas d'ingrédients. )
                </Typography.Text>
              );
            }}
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button htmlType="button" style={{ margin: '0 8px' }} onClick={showIngredientModal}>
              Ajouter un ingrédient
            </Button>
          </Form.Item>

          <Form.Item label="Description" rules={[{ required: true }]}>
            <Input.TextArea name="recipeDescription" value={form.recipeDescription} onChange={handleFormChange} disabled={loading}/>
          </Form.Item>

          <Form.Item {...tailLayout}>
              <Button htmlType="submit" type="primary">
                  Enregistrer
              </Button>
          </Form.Item>
        </Form>
        <ModalForm visible={visible} onCancel={hideIngredientModal} />
      </Form.Provider>
    </Content>
  )
}
