import { Col, Pagination, Row } from "antd";
import { Content } from "antd/lib/layout/layout";
import { useContext } from "react";
import { useQuery } from "react-query";
import { useParams } from "react-router";

import { recipesList } from "../api/recipeApi";
import Card from "../components/Card/Card";
import SearchBar from "../components/Search/SearchBar";
import { SkeletonHomeRecipesList } from "../components/Skeleton/SkeletonHome";
import { GlobalStateContext } from "../context/GlobalState";

export default function RecipesList() {
    let {page: pageNumber} = useParams();
    const {value} = useContext(GlobalStateContext);
    const {data, isLoading} = useQuery(["/", pageNumber, value.pageSize], () => {
        if(!pageNumber){
            throw new Error("Pas de numéro ou de taille de pages")
        }
        //pageNumber = parseInt(pageNumber);
        //const pageSize = parseInt(param.size);
        return recipesList(parseInt(pageNumber), value.pageSize)
    });
    
    return (<Content style={{ padding: '50px' }}>{
        isLoading 
            ? <SkeletonHomeRecipesList /> 
            : (<>
                <SearchBar />
                <Row gutter={[25,40]}>{
                    data?.map((recipe, index: number) => (
                        <Col key={index} span={6}>
                            <Card item={recipe} />
                        </Col>
                    ))
                }
                </Row>
                <Pagination 
                defaultCurrent={1} 
                //total={24} 
                size="small"
                //pageSize={12}
                />
            </>)
    }</Content>)
}